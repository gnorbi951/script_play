import random
import requests
import time
import asyncio
import aiohttp


MAX_CONCURRENT_REQUESTS = 2


# Define the async function to send requests and measure response times
async def send_request(_url, sem):
    async with sem:
        async with aiohttp.ClientSession() as session:
            start_time = time.perf_counter()
            async with session.get(_url) as response:
                result = await response.text()
                end_time = time.perf_counter()
                # if (end_time - start_time) * 1000 < 5:
                print(response)
                return (end_time - start_time) * 1000


# Define the async function to send multiple requests
async def main():
    sem = asyncio.Semaphore(MAX_CONCURRENT_REQUESTS)
    urls = create_urls(5)
    results = []
    tasks = [asyncio.ensure_future(send_request(_url, sem)) for _url in urls]
    return await asyncio.gather(*tasks)


def create_urls(num):
    urls = []
    for _ in range(num):
        url = 'https://google.com'
        urls.append(url)
    return urls


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    response_times = loop.run_until_complete(main())
    with open('./response_times.csv', 'w') as f:
        transformed_data = list(map(lambda x: str(x), response_times))
        for item in transformed_data:
            f.write(f'{round(float(item), 2)}\n')

